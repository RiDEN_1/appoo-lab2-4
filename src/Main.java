import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ShootingRange shootingRange = new ShootingRange();
        Scanner in = new Scanner(System.in);
        double initialSkill = 0;
        while (true) {
            System.out.println("1.Add AR\n" +
                    "2.Add piston\n" +
                    "3.Enter initial skill level\n" +
                    "4.Shoot on the range\n" +
                    "5.Shorten barrel\n" +
                    "6.Put on/off silencer\n" +
                    "7.Clean guns\n" +
                    "x.Exit");
            GunFactory gunFactory = new GunFactory();
            switch (in.nextLine()) {
                case "1":
                    shootingRange.addGun(gunFactory.getGun("assault rifle", in));
                    break;
                case "2":
                    shootingRange.addGun(gunFactory.getGun("pistol", in));
                    break;
                case "3":
                    initialSkill = Double.parseDouble(in.nextLine());
                    break;
                case "4":
                    shootingRange.start(initialSkill);
                    break;
                case "5":
                    System.out.println("enter amount");
                    int shortening = Integer.parseInt(in.nextLine());
                    System.out.println("pick weapon:\n" +
                            shootingRange.getGunsList().toString());
                    shootingRange.getAR(Integer.parseInt(in.nextLine())).shortenBarrel(shortening);
                    break;
                case "6":
                    System.out.println("pick weapon:\n" +
                            shootingRange.getGunsList().toString());
                    shootingRange.getPistol(Integer.parseInt(in.nextLine())).changeSilencerState();
                    break;
                case "7":
                    System.out.println("pick weapon:\n" +
                            shootingRange.getGunsList().toString());
                    Guns currentGun = shootingRange.getGun(Integer.parseInt(in.nextLine()));
                    System.out.println("1.Full Maintenance\n" +
                            "2.Adjust\n" +
                            "3.Clean\n");
                    GunMaintenance.gunMaintenance(currentGun, Integer.parseInt(in.nextLine()));
                    break;
                case "x":
                    return;
            }
        }
    }
}
