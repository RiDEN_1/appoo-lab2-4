import java.util.ArrayList;
import java.util.List;

public class ShootingRange {
    private final List<Guns> gunsList = new ArrayList<>();

    public void addGun(Guns gun) {
        gunsList.add(gun);
    }

    public List<Guns> getGunsList() {
        return gunsList;
    }

    public Pistol getPistol(int i) {
        if (gunsList.get(i) instanceof Pistol) return (Pistol) gunsList.get(i);
        else return null;
    }

    public AssaultRifle getAR(int i) {
        if (gunsList.get(i) instanceof AssaultRifle) return (AssaultRifle) gunsList.get(i);
        else return null;
    }
    public Guns getGun(int i) {
        if (!gunsList.isEmpty()) return gunsList.get(i);
        else return null;
    }

    public void start(double initialSkill) {
        for (Guns curGun :
                gunsList) {
            curGun.shoot(initialSkill + Math.random());
        }
    }
}
