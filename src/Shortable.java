public interface Shortable {
    void    shortenBarrel(double shorteningAmount);
}
