import java.util.Scanner;

import static java.lang.Math.pow;

public class AssaultRifle extends Guns implements Shortable{
    private double barrelLength;

    public double getBarrelLength() {
        return barrelLength;
    }

    public void setBarrelLength(double barrelLength) {
        this.barrelLength = barrelLength;
    }

    public AssaultRifle(double gauge, double initialVelocity, double bulletMass, double barrelLength) {
        super(gauge, initialVelocity, bulletMass);
        this.barrelLength = barrelLength;
    }

    public AssaultRifle(Scanner in) {
        super(in);
        System.out.println("Enter barrel length");
        barrelLength = Double.parseDouble(in.nextLine());
    }

    @Override
    public void shoot(double shooterSkillLevel) {
        double score = (getGauge() * 1200 + getBulletMass() * pow(getInitialVelocity(), 2)
                + getBarrelLength() * 1000 * getAccuracy() * getDirtyness()) / 1000 ;
        setDirtyness(getDirtyness()*0.8);
        setAccuracy(getAccuracy()*0.9);
        System.out.println("Score is:  " + score);
    }

    @Override
    public void shortenBarrel(double shorteningAmount) {
        barrelLength-=shorteningAmount;
    }

}
