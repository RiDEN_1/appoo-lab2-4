import java.util.Scanner;

public abstract class Guns {
    private double gauge;
    private double initialVelocity;
    private double bulletMass;
    private double dirtyness = 1;
    private double accuracy = 1;

    public double getDirtyness() {
        return dirtyness;
    }

    public void setDirtyness(double dirtyness) {
        this.dirtyness = dirtyness;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public Guns(double gauge, double initialVelocity, double bulletMass) {
        this.gauge = gauge;
        this.initialVelocity = initialVelocity;
        this.bulletMass = bulletMass;
    }
    public Guns(Scanner in){
        System.out.println("Enter gauge");
        gauge = Double.parseDouble(in.nextLine());
        System.out.println("Enter initial velocity");
        initialVelocity = Double.parseDouble(in.nextLine());
        System.out.println("Enter bullet mass");
        bulletMass = Double.parseDouble(in.nextLine());
    }

    public double getGauge() {
        return gauge;
    }

    public void setGauge(double gauge) {
        this.gauge = gauge;
    }

    public double getInitialVelocity() {
        return initialVelocity;
    }

    public void setInitialVelocity(double initialVelocity) {
        this.initialVelocity = initialVelocity;
    }

    public double getBulletMass() {
        return bulletMass;
    }

    public void setBulletMass(double bulletMass) {
        this.bulletMass = bulletMass;
    }


    public abstract void shoot(double shooterSkillLevel);
}
