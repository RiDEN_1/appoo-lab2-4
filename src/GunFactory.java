import java.util.Scanner;

public class GunFactory {
    public Guns getGun(String gunType, Scanner in) {
        switch (gunType.toLowerCase()) {
            case "assault rifle":
                return new AssaultRifle(in);
            case "pistol":
                return new Pistol(in);
            default:
                return null;
        }
    }
}
