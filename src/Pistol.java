import java.util.Scanner;

import static java.lang.Math.pow;

public class Pistol extends Guns implements Silenceable {
    private boolean hasSilencer;

    public boolean isHasSilencer() {
        return hasSilencer;
    }

    public void setHasSilencer(boolean hasSilencer) {
        this.hasSilencer = hasSilencer;
    }

    public Pistol(double gauge, double initialVelocity, double bulletMass) {
        super(gauge, initialVelocity, bulletMass);
    }

    public Pistol(Scanner in) {
        super(in);
        System.out.println("Does it have a silencer?");
        this.hasSilencer = Boolean.parseBoolean(in.nextLine());
    }

    @Override
    public void shoot(double shooterSkillLevel) {
        double score = (getGauge() * 1200 + getBulletMass() * pow(getInitialVelocity(), 2)
                + (isHasSilencer()?-500.0:100.0))  * getAccuracy() * getDirtyness() / 1000;
        setDirtyness(getDirtyness()*0.75);
        setAccuracy(getAccuracy()*0.85);
        System.out.println("Score is:  " + score);
    }

    @Override
    public void changeSilencerState() {
        hasSilencer=!hasSilencer;
    }
}
